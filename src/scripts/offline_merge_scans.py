#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os


#####################################
# Load data
#####################################

# Lidar laser
df_lidar = pd.read_csv('/home/sofia/catkin_ws/src/visual/data/laserscan.csv', header=0)
col_lidar = [col for col in df_lidar if col.startswith('field.ranges')]
lidar_range = df_lidar[col_lidar].values.astype('float32') 
lidar_range = np.nan_to_num(lidar_range)
#lidar_range = np.nan_to_num(lidar_range, posinf=0) ####
lidar_range = np.transpose(lidar_range)


# Camera laser
df_camera = pd.read_csv('/home/sofia/catkin_ws/src/visual/data/depth2scan.csv', header = 0)
col_camera = [col for col in df_camera if col.startswith('field.ranges')]
camera_range = df_camera[col_camera].values.astype('float32') 
camera_range = np.nan_to_num(camera_range)

camera_range = camera_range[0:lidar_range.shape[1], :] # Maybe not needed
#camera_range = np.nan_to_num(camera_range, posinf=0) ####
camera_range = np.transpose(camera_range)

fov = 135
lid = lidar_range[:, 0]
cam = camera_range[:, 0]
lid = lid[fov:360 - fov]

print('lid', lid.shape, 'cam', cam.shape)



#####################################
# Load dict and mean the values
#####################################

indexes = np.load('/home/sofia/catkin_ws/src/visual/data/dict.npy',allow_pickle='TRUE').item()
    
print(type(indexes), indexes[0]['cam'][0], len(indexes))

merged_scan = []

for i in range(lid.shape[0]):
    temp1 = cam[indexes[i]['cam'][0]] 
    temp2 = lid[indexes[i]['lid'][0]]
    temp = np.concatenate((temp1, temp2))
    merged_scan.append(np.mean(temp))

print(len(merged_scan), merged_scan[0:3])