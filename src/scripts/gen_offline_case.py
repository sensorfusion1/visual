#!/usr/bin/env python3
import sys
import os
labeling_dir = os.path.expanduser('~/labeling')
new_network_dir = os.path.expanduser('~/new_network')
sys.path.append(labeling_dir)
from load_npy_data import load_npy_case
sys.path.append(new_network_dir+'/scripts')
from help_functions import clean_data
import numpy as np
print("Generate case data for testing without AE")

to_dir = os.path.expanduser('~/catkin_ws/src/visual/data')

case = load_npy_case('Aa','nc1')
print("case shape", case.shape)

clean_case = clean_data(case)
print("clean case shape", clean_case.shape, "max clean reading", np.amax(clean_case))

np.save(os.path.expanduser(to_dir+'/fake_rec_AE.npy'), clean_case) 