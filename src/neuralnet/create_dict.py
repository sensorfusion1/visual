#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate
from keras.models import Model
import tensorflow as tf


from keras.utils.vis_utils import plot_model

print('hello')

# Import data
camera_range = np.load('src/visual/data/camera_range.npy')
lidar_range = np.load('src/visual/data/lidar_range.npy')
detect_label = np.load('src/visual/data/labels.npy')

print('input shapes: ',camera_range.shape, lidar_range.shape)


# Set angles
lidar_angle_min = -3.1241390705108643
lidar_angle_max = 3.1415927410125732
lidar_angle_increment = 0.01745329238474369



camera_angle_min = -0.7669617533683777
camera_angle_max = 0.7851662635803223
camera_angle_increment = 0.0036693334113806486

lid = lidar_range[0, :]
cam = camera_range[0, :]
print('lid', lid.shape, 'cam', cam.shape)



## Create a list of all the corresponding angles for the camera measurements
cam_angles = []
ang = camera_angle_min

while ang <= camera_angle_max + camera_angle_increment:
    cam_angles.append(ang)
    ang = ang + camera_angle_increment


## Similarly list of angles for the lidar measurements
lid_angles = []
ang = lidar_angle_min

while ang <= lidar_angle_max:
    lid_angles.append(ang)
    ang = ang + lidar_angle_increment

## Create the list with which we will sort the arrays
## This list includes a lower and upper angle for sorting of type a < X <= b
grid_ang = []
ang = lidar_angle_min - lidar_angle_increment / 2

while ang <= lidar_angle_max + lidar_angle_increment / 2:
    grid_ang.append(ang)
    ang = ang + lidar_angle_increment



# Grid list for sorting camera angles
# cam_grid = [camera_angle_min + camera_angle_increment / 2]
# for ang in cam_angles:
#     cam_grid.append(ang + camera_angle_increment / 2)



# The created lists:
print('lenghts: ', len(cam_angles), len(lid_angles),  len(grid_ang))

cam_angles = np.asarray(cam_angles)
lid_angles = np.asarray(lid_angles)
grid_ang = np.asarray(grid_ang)


print('cam: ', cam_angles[0], cam_angles[-1])
print('lid: ', lid_angles[0], lid_angles[-1])
print('grid: ', grid_ang[0], grid_ang[-1])

np.save('src/visual/data/lid_angles.npy', lid_angles) 
np.save('src/visual/data/cam_angles.npy', cam_angles) 
np.save('src/visual/data/grid_angles.npy', grid_ang) 


# Create a dict where each entry contains the indexes for camera and lidar angles
# that lies between the values in the grid_ang list.
# Intervals of 1 degree (same as lidar)


indexes = dict()
for i in range(lid_angles.shape[0]):
    cam_indexes = np.where(np.logical_and(cam_angles > grid_ang[i], cam_angles <= grid_ang[i+1]))
    lid_indexes = np.where(np.logical_and(lid_angles > grid_ang[i], lid_angles <= grid_ang[i+1]))
    indexes[i] = {'cam': cam_indexes, 'lid': lid_indexes}

print(len(indexes))
np.save('src/visual/data/dict.npy', indexes) 


    



