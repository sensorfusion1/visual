#!/usr/bin/env python3

print('Autoencoder starting')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import rospy
from sensor_msgs.msg import Image, LaserScan
from std_msgs.msg import String, Header, Int32
import numpy as np
import keras
import message_filters
import threading




################################################
# Topics
################################################
# Image topic camera, width 640, height = 480
cam_topic = "/cam2lid"

# Scan topic lidar, start angle of scan: -pi, end angle of scan +pi. 
# 360 points per scan
# range min 0.15m, range max 12m
# intensities: 47 if reflective surface, 0 if not
lid_topic = "/scan"

# Autoencoder result topic to publish string
autoenc_res = "/autoencoder_result"


################################################
## Load autoencoder model
################################################

network_dir = os.path.expanduser('~/new_network')
eval_dir = os.path.expanduser('~/eval')
detector = keras.models.load_model(network_dir + '/detector_model/detector_lim_batches')

# For measuring time
start = None
end = None

exec_file_name = 'no_AE_Aa_nc1'

f= open(eval_dir + "/exec_time/" + exec_file_name + ".csv","w+")
f.write('Class'+'\t'+ 'Time (s)'+ "\n")
f.close()
    
def callback(cam_data, lid_data):

    print('-----------------')

    start = rospy.Time.now()

    cam_ranges = np.asarray(cam_data.ranges)
    lid_ranges = np.asarray(lid_data.ranges)
    # print(cam_ranges.shape, lid_ranges.shape)

    # Prep for input into AE
    data = np.concatenate((cam_ranges, lid_ranges), axis=0)
    data = np.nan_to_num(data, nan=1e10)

    
    ins_lid = np.where(data > 20)
    for j in ins_lid[0]:
        data[j] = data[j-1]
        
    ins_cam = np.where(data[:424] < 0.15)
    for i in ins_cam[0]:
        data[i+1] = data[ i]


    data = np.reshape(data, (1, 784))
  

    # Perform prediction
    predict = detector.predict(data)


    print('Predicted class: ', np.argmax(predict[0]) + 1)

    pub_res = rospy.Publisher(autoenc_res, Int32, queue_size=1)
    # pub_res.publish(1)
    pub_res.publish(np.argmax(predict[0]) + 1)

    

    end = rospy.Time.now()
    exec_time = end-start

    print('exec: ', end-start)

    f= open(network_dir + "/eval/exec_time/" + exec_file_name + ".csv","a+")
    f.write(str(np.argmax(predict[0]) + 1)+'\t'+ str(exec_time.to_sec())+ "\n")
    f.close()



    
def main():

    rospy.init_node('autoencoder', anonymous=False)


    cam_sub = message_filters.Subscriber(cam_topic, LaserScan)
    lid_sub = message_filters.Subscriber(lid_topic, LaserScan)

    

    ts = message_filters.ApproximateTimeSynchronizer([cam_sub, lid_sub], 10, 0.05)
    # Create a thread from a function with arguments
    # while not rospy.is_shutdown():
    try:
        threading.Thread(target=ts.registerCallback(callback)).start()
    except rospy.ROSInterruptException:
        pass
    
    # Start the thread
    # ts.registerCallback(ae)

    

    rospy.spin()

if __name__ == '__main__':
    main()

