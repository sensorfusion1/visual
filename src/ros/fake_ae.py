#!/usr/bin/env python3

print('hello')

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import String, Int32
import numpy as np



def talker():

    pub = rospy.Publisher('/autoencoder_result', Int32, queue_size=10)
    rospy.init_node('talker', anonymous=True)

    rate = rospy.Rate(7.5) # 10hz
    while not rospy.is_shutdown():

        # res = np.random.randint(1, 5)
        res = 1
        # Publish both are OK for merger
        pub.publish(res)
        rate.sleep()


if __name__ == '__main__':
    # listener()
    talker()

# Point cloud topic camera, width ~1325, height = 1
# /orb_slam2_mono/map_points

# Point cloud topic lidar, width ~145, heigth = 1
# /scan_matched_points2
