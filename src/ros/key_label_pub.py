#!/usr/bin/env python3
import rospy
from visual.msg import time_str,HeaderString2
import time
import sys

def talker():
    while True:
        try:
            pub = rospy.Publisher('/key_label_pub', time_str, queue_size=10)
            rospy.init_node('key_label_publisher', anonymous=True)

            string_input = input("input: ")
            if string_input == "1" or string_input == "2" or string_input == "3":
                msg = time_str()
                header = HeaderString2(stamp=rospy.Time.now())
                msg.header = header
                msg.string_data = string_input
                pub.publish(msg)
        except KeyboardInterrupt:
            sys.exit()

    rospy.spin()

if __name__ == '__main__':
    print("main")
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
