#!/usr/bin/env python3

print('Autoencoder starting')

import rospy
from sensor_msgs.msg import Image, LaserScan
from std_msgs.msg import String
import numpy as np
import keras
import os

################################################
# Topics
################################################
# Image topic camera, width 640, height = 480
cam_topic = "/cam2lid"

# Scan topic lidar, start angle of scan: -pi, end angle of scan +pi. 
# 360 points per scan
# range min 0.15m, range max 12m
# intensities: 47 if reflective surface, 0 if not
lid_topic = "/scan"

# Autoencoder result topic to publish string
autoenc_res = "/autoencoder_result"
rec_cam = "/rec_cam"
rec_lid = "/rec_lid"

################################################
## Load autoencoder model
################################################
network_dir = os.path.expanduser('~/network')
model = keras.models.load_model(network_dir + '/models/' + 'dense_small_221_10epochs_FalseFalseFalsequant_camlid')

class handler:
    def __init__(self):
        self.camera = None
        self.lidar = None
        self.lock = False

    def camera_callback(self, data):
        if not self.lock:
            self.camera = data
        
    def lidar_callback(self, data):
        self.lidar = data
        if not (self.lidar and self.camera) == None and not self.lock:  
            self.auto_encoder(self.camera, self.lidar)

    def auto_encoder(self, array):
        self.lock = True
        print('------------------Autoencoder-------------------')



        cam_ranges = np.asarray(self.camera.ranges) # (360,) column vector
        lid_ranges = np.asarray(self.lidar.ranges) # (360,) column vector
        cam_ranges = np.reshape(intens, (360, 1))
        lid_ranges = np.reshape(ranges, (360, 1))
        
        rec = model.predict([cam_ranges, lid_ranges])

        print(im.shape, intens.shape, ranges.shape)
        np.savez(npz_path, im=im, intens=intens, ranges=ranges)


        resp = rospy.Publisher(autoenc_res, String, queue_size=1)
        resp.publish('3')
        self.lock = False
    
def main():


    rospy.init_node('autoencoder', anonymous=False)

    ha = handler()
    rospy.Subscriber(cam_topic, Image, ha.camera_callback)
    rospy.Subscriber(lid_topic, LaserScan, ha.lidar_callback)

    rospy.spin()

if __name__ == '__main__':
    main()

