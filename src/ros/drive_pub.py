#!/usr/bin/env python3

# Import required modules
import time
from visual.srv import *
from visual.msg import time_str,HeaderString2
import rospy


def drive_server():
    
    pub = rospy.Publisher('drive_cmd', time_str, queue_size=10)
    rospy.init_node('drive_pub')
    while True:
        try:
            x = str(input("Provide driving input: "))
            msg = time_str()
            header = HeaderString2(stamp=rospy.Time.now())
            msg.header = header
            msg.string_data = x
            pub.publish(msg)
            #drive_output = handle_drive(x)
            #print("input:",x , drive_output)
        except KeyboardInterrupt:
            sys.exit()
if __name__ == '__main__':
    try:
        drive_server()
    except rospy.ROSInterruptException:
        pass

