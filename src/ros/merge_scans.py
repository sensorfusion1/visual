#!/usr/bin/env python3

print('Merge the two laser scans')
# Node that listens to raw laserscan messages from lidar and from depth image
# It then clips the scan messages to a specific length and puts them through autoencoder
# The reconstructed scans are published to new topics /auto_*

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Int32, Header, String
import numpy as np
import time
import os
import message_filters



lid_topic = "/scan"       
cam_topic = "/cam2lid" 
ae_topic = "/autoencoder_result"

# Scan topics to publish to 
merged_scan_top = "/merged_scan"



lidar_angle_min = -3.1241390705108643
lidar_angle_increment = 0.01745329238474369
lidar_angle_max = 3.1415927410125732


camera_angle_min = -0.7669617533683777
camera_angle_max = 0.7851662635803223
cam_angle_increment = 0.0036693334113806486


cam_angles = np.arange(camera_angle_min, camera_angle_max + cam_angle_increment, cam_angle_increment)
cam_grid = np.arange(camera_angle_min - 0.5*cam_angle_increment, camera_angle_max + cam_angle_increment, cam_angle_increment)

lid_angles = np.arange(lidar_angle_min, lidar_angle_max + lidar_angle_increment, lidar_angle_increment)
lid_grid = np.arange(lidar_angle_min - 0.5 * lidar_angle_increment, lidar_angle_max + lidar_angle_increment, lidar_angle_increment)

# Bin up the angles according to lidar grid
digi_cam = np.digitize(cam_angles, lid_grid)
digi_lid = np.digitize(lid_angles, lid_grid)


def callback(cam_data, lid_data, detect):
    
    print('-----------------')
    print('headers', lid_data.header.seq, cam_data.header.seq, 'result', detect )


    msg = LaserScan()

    lid = np.asarray(lid_data.ranges)
    cam = np.asarray(cam_data.ranges)

    if detect == Int32(1) or detect == Int32(4):

        print('merged both scans evenly')
        

        # Mean of each bin for camera and lidar respectively
        bin_cam = [np.nanmean(cam[digi_cam == i]) for i in range(1, len(lid_grid)+1)]
        bin_lid = [np.nanmean(lid[digi_lid == i]) for i in range(1, len(lid_grid)+1)]

        # Stack and take mean of each entry
        merged_scans = np.nanmean(np.vstack((bin_lid, bin_cam)), axis=0)


        

    elif detect == Int32(2):

        print('merged only with lidar')

        merged_scans = np.asarray(lid_data.ranges)
        

        

    elif detect == Int32(3):
        print('merged with camera over lidar')

        # only use camera
        merged_scans = np.asarray(lid_data.ranges)
        cam = np.asarray(cam_data.ranges)

        bin_cam = [np.nanmean(cam[digi_cam == i]) for i in range(1, len(lid_grid)+1)]
        
        cam_inds = list(np.where(np.isfinite(bin_cam))[0])

        merged_scans[cam_inds[0]] = bin_cam[cam_inds[0]]





    pub_scan = rospy.Publisher(merged_scan_top, LaserScan, queue_size=1)
    header = Header(frame_id='laser', stamp=rospy.Time.now())
    msg.header = header
    msg.range_min = lid_data.range_min
    msg.range_max = lid_data.range_max
    msg.angle_min = lidar_angle_min
    msg.angle_max = lidar_angle_max
    msg.angle_increment = lidar_angle_increment
    msg.time_increment = lid_data.time_increment
    msg.ranges = tuple(merged_scans)

    pub_scan.publish(msg)
    


    
def main():

    rospy.init_node('merge_scans', anonymous=False)

    print('main')

    cam_sub = message_filters.Subscriber(cam_topic, LaserScan)
    lid_sub = message_filters.Subscriber(lid_topic, LaserScan)
    res_sub = message_filters.Subscriber(ae_topic, Int32)


    ts = message_filters.ApproximateTimeSynchronizer([cam_sub, lid_sub, res_sub], 20, 0.5, allow_headerless=True)
    ts.registerCallback(callback)
    
    rospy.spin()

if __name__ == '__main__':
    main()

