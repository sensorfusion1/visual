#!/usr/bin/env python3

from __future__ import print_function
import sys
import rospy
import time
from rpi_package.srv import *
from visual.msg import time_str,HeaderString2

def drive_client(x):
    rospy.wait_for_service('drive_command')
    try:
        drive_resp = rospy.ServiceProxy('drive_command', Drive)
        resp1 = drive_resp(x)
        return resp1
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def command_input():
    pub = rospy.Publisher('drive_cmd', time_str, queue_size=10)
    while True:
        try:
            x = str(input("Provide driving input: "))
#            msg = time_str()
#            header = HeaderString2(stamp=rospy.Time.now())
#            msg.header = header
#            msg.string_data = x
#            pub.publish(msg)
            print("input:",x , drive_client(x))
        except KeyboardInterrupt:
            sys.exit()
    

if __name__ == "__main__":
    rospy.init_node('drive_client')

    command_input()
    rospy.spin()

