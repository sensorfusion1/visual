#!/usr/bin/env python3

print('Merge the two laser scans')
# Node that listens to raw laserscan messages from lidar and from depth image
# It then clips the scan messages to a specific length and puts them through autoencoder
# The reconstructed scans are published to new topics /auto_*

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Int32, Header
import numpy as np
import time
import os

# Directories
dict_dir = os.path.expanduser('~/catkin_ws/src/visual/data/dict.npy')

# Scan topics to listen to
# lid_topic = "/scan"       
# cam_topic = "/cam2lid" 
lid_topic = "/scan"       
cam_topic = "/cam2lid" 

# Scan topics to publish to 
merged_scan_top = "/merged_scan"



lidar_angle_min = -3.1241390705108643
lidar_angle_increment = 0.01745329238474369
lidar_angle_max = 3.1415927410125732


camera_angle_min = -0.7669617533683777
camera_angle_max = 0.7851662635803223
cam_angle_increment = 0.0036693334113806486


cam_angles = np.arange(camera_angle_min, camera_angle_max + cam_angle_increment, cam_angle_increment)
cam_grid = np.arange(camera_angle_min - 0.5*cam_angle_increment, camera_angle_max + cam_angle_increment, cam_angle_increment)

lid_angles = np.arange(lidar_angle_min, lidar_angle_max + lidar_angle_increment, lidar_angle_increment)
lid_grid = np.arange(lidar_angle_min - 0.5 * lidar_angle_increment, lidar_angle_max + lidar_angle_increment, lidar_angle_increment)



def camera_callback(cam_data):   
    
    rospy.Subscriber(lid_topic, LaserScan, lidar_callback, cam_data, True)         

    
def lidar_callback(lid_data, cam_data):
    pub_scan = rospy.Publisher(merged_scan_top, LaserScan, queue_size=1)

    print('headers', lid_data.header.seq, cam_data.header.seq)

    # TODO Create handler for subscribing to classification
    detect = 1 # Fake result from detector

    msg = LaserScan()
    

    if detect == 1:

        # Use both scans
        lid = np.asarray(lid_data.ranges)
        cam = np.asarray(cam_data.ranges)

        # Bin up the angles according to lidar grid
        digi_cam = np.digitize(cam_angles, lid_grid)
        digi_lid = np.digitize(lid_angles, lid_grid)

        # Mean of each bin for camera and lidar respectively
        bin_cam = [np.nanmean(cam[digi_cam == i]) for i in range(1, len(lid_grid))]
        bin_lid = [np.nanmean(lid[digi_lid == i]) for i in range(1, len(lid_grid))]

        # Stack and take mean of each entry
        merged_scans = np.nanmean(np.vstack((bin_lid, bin_cam)), axis=0)

        msg.angle_min = lidar_angle_min
        msg.angle_max = lidar_angle_max
        msg.angle_increment = lidar_angle_increment
        msg.time_increment = lid_data.time_increment

        print('merged both scans', merged_scans.shape)

    elif detect == 2:

        # Only use lidar
        merged_scans = np.asarray(lid_data.ranges)
        print('merged only with lidar', merged_scans.shape)

        msg.angle_min = lidar_angle_min
        msg.angle_max = lidar_angle_max
        msg.angle_increment = lidar_angle_increment
        msg.time_increment = lid_data.time_increment

    elif detect == 3:

        # only use camera
        merged_scans = np.asarray(cam_data.ranges)
        print('merged only with camera', merged_scans.shape)

        msg.angle_min = camera_angle_min
        msg.angle_max = camera_angle_max
        msg.angle_increment = cam_angle_increment
        msg.time_increment = cam_data.time_increment

    elif detect == 4:

        # Publish empty arras
        merged_scans = np.empty_like(lid_data.ranges)
        print('merged scan empty', merged_scans.shape)

        msg.angle_min = lidar_angle_min
        msg.angle_max = lidar_angle_max
        msg.angle_increment = lidar_angle_increment
        msg.time_increment = lid_data.time_increment

    
    header = Header(frame_id='laser', stamp=rospy.Time.now())
    msg.header = header
    msg.range_min = lid_data.range_min
    msg.range_max = lid_data.range_max
    msg.ranges = tuple(merged_scans)

    pub_scan.publish(msg)
    print('merger', header.stamp)


    
def main():

    rospy.init_node('merge_scans', anonymous=False)
    rospy.Subscriber(cam_topic, LaserScan, camera_callback)
    
    rospy.spin()

if __name__ == '__main__':
    main()

