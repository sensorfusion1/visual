#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import LaserScan
import numpy as np
import time
import pandas as pd
from std_msgs.msg import Int32, Header
import os

# Scan topic listen
cam_topic = "depth2scan"
pub_cam_topic = "cam2lid"


a = 17.5e-3  # Shift from lidar's center to cameras center in x
b = 135e-3   # distance between lidar's center and camera screw in y


# cam_angles = np.load(os.path.expanduser('~/catkin_ws/src/visual/data/cam_angles.npy'))
# cam_grid = np.load(os.path.expanduser('~/catkin_ws/src/visual/data/cam_grid.npy'))


camera_angle_min = -0.7669617533683777
camera_angle_max = 0.7851662635803223
cam_angle_increment = 0.0036693334113806486


cam_angles = np.arange(camera_angle_min, camera_angle_max + cam_angle_increment, cam_angle_increment)
cam_grid = np.arange(camera_angle_min - 0.5*cam_angle_increment, camera_angle_max + cam_angle_increment, cam_angle_increment)


def camera_callback(data):
    

    pub_scan = rospy.Publisher(pub_cam_topic, LaserScan, queue_size=1)
    #print("here***************************************************")
    
    r_cam = np.asarray(data.ranges) #range from depth2scan

    # Remove all NANs    
    r_cam = np.nan_to_num(r_cam)


    # r_c * sin(theta_c) & r_c * cos(theta_c)
    cam_x = np.multiply(np.sin(cam_angles),r_cam)
    cam_y = np.multiply(np.cos(cam_angles),r_cam)


    # X part of lidar frame coordinate (left and right)
    # Y part (depth in image)
    lid_x = a + cam_x
    lid_y = b + cam_y
    
    # Camera range according to lidar frame
    # Angles in lidar frame
    # r_lid = np.sqrt(lid_x**2 + lid_y**2)
    r_lid = (lid_x**2 + lid_y**2)**0.5
    theta_lid = np.arcsin(lid_x/r_lid)

    # Bin up the angles according to cam grid so it is evenly distributed
    # digi_cam = np.digitize(theta_lid, cam_grid)
    digi_cam = np.searchsorted(cam_grid, theta_lid,side='right')

    # Sort the ranges in lidars frame according to the digi cam bins
    sorted_scan = [np.nanmean(r_lid[digi_cam == i]) for i in range(1, len(cam_grid))]


    print(len(sorted_scan))
    
    msg = LaserScan()
    header = Header(frame_id='laser', stamp=rospy.Time.now())
    msg.header = header
    msg.angle_min = cam_angles[0]
    msg.angle_max = cam_angles[-1]
    msg.angle_increment = cam_angle_increment
    msg.time_increment = data.time_increment
    msg.scan_time = data.scan_time
    msg.range_min = 0.15
    msg.range_max = 12
    msg.ranges = tuple(r_lid)

    pub_scan.publish(msg)
    print("pub")
    
        
def main():

    print("main")

    rospy.init_node('frame_cam2lid_node', anonymous=False)
    rospy.Subscriber(cam_topic, LaserScan, camera_callback)
 
    rospy.spin()

if __name__ == '__main__':
    main()
