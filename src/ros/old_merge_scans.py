#!/usr/bin/env python3

print('Merge the two laser scans')
# Node that listens to raw laserscan messages from lidar and from depth image
# It then clipps the scan messages to a specific length and puts them through autoencoder
# The reconstructed scans are published to new topics /auto_*

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Int32, Header
import numpy as np
import time
import os



# Scan topics to listen to
lid_topic = "/scan"       
cam_topic = "/cam2lid" 

# Scan topics to publish to 
auto_lid = "/merged_scan"
auto_cam = "/auto_depth2scan"

# field of view adjustment for the lidar
#
# angle_min, angle_max, angle_increment
# lidar -3.1241390705108643 3.1415927410125732 0.01745329238474369
# camera:  -0.7669617533683777 0.7851662635803223 0.0036693334113806486
# lidar_angle_min + fov * lidar_angle_increment = camera_angle_min
lidar_angle_min = -3.1241390705108643
lidar_angle_increment = 0.01745329238474369
lidar_angle_max = 3.1415927410125732


cwd = os.getcwd()
indexes = np.load(os.path.expanduser('~/catkin_ws/src/visual/data/dict.npy'),allow_pickle='TRUE').item()

#indexes = np.load(cwd+'/src/visual/data/dict.npy',allow_pickle='TRUE').item()
print('dict: ', len(indexes))


class handler:
    def __init__(self):
        self.camera = None
        self.lidar = None
        self.lock = False

    def camera_callback(self, data):            ## TODO arrange timings
        #if not self.lock:
            self.camera = data
       
        
    def lidar_callback(self, data):
        #if not self.lock:
            self.lidar = data
            if (self.camera != None and self.lidar != None): 
                self.merger()


    def merger(self):
        self.lock = True
        pub_scan = rospy.Publisher(auto_lid, LaserScan, queue_size=1)

        lid = np.asarray(self.lidar.ranges)
        print('lidar shape', lid.shape)

        cam = np.asarray(self.camera.ranges)

        merged_scan = []
        for j in range(lid.shape[0]-1):
            temp_cam, temp_lid = cam[indexes[j]['cam'][0]] , lid[indexes[j]['lid'][0]]
            temp = np.hstack((np.mean(temp_cam[np.where(temp_cam < 20)]) , temp_lid[np.where(temp_lid < 20)]))
            temp = temp[np.where(temp < 20)] # Ignore values outside max range
            merged_scan.append(np.mean(temp))

        print('merged', len(merged_scan))
 

        # TODO
        msg = LaserScan()
        header = Header(frame_id='laser', stamp=rospy.Time.now())
        msg.header = header
        msg.angle_min = lidar_angle_min
        msg.angle_max = lidar_angle_max
        msg.angle_increment = lidar_angle_increment
        msg.time_increment = self.lidar.time_increment
        msg.range_min = self.lidar.range_min
        msg.range_max = self.lidar.range_max
        msg.ranges = tuple(merged_scan)


        pub_scan.publish(msg)
        #self.lock = False
        print('merger', header.stamp)

    
def main():

    rospy.init_node('merge_scans', anonymous=False)

    ha = handler()
    rospy.Subscriber(cam_topic, LaserScan, ha.camera_callback)
    rospy.Subscriber(lid_topic, LaserScan, ha.lidar_callback)
 
    rospy.spin()

if __name__ == '__main__':
    main()

